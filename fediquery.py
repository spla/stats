import sys
import os
import os.path
import re
from datetime import datetime, timedelta
from app.libraries.setup import Setup
from mastodon import Mastodon, StreamListener
from app.libraries.database import Database
from app.libraries.nodeinfo import Nodeinfo
from app.libraries.peers import Peers
from app.libraries.mentions import Mentions
import httpx
import pdb

class Listener(StreamListener):

    def on_notification(self, notification):

        if notification.type != 'mention':

            return

        else:

            qry = Mentions(mastodon)

            reply, query_word, username, status_id, visibility = qry.get_data(notification)

            if reply == True:

                if query_word[:4] == 'soft':

                    key_word = query_word[:4]

                    search_soft = query_word[5:]

                    if search_soft != '':

                        servers, users, mau = db.get_soft_data(search_soft)

                        toot_text = f'@{username}, my data for {search_soft} software:\n\n'

                        if servers != 0:

                            toot_text += f'software :{search_soft}:\nservers: {servers:,}\nusers: {users:,}\nMAU: {mau:,}'

                        else:

                            toot_text += 'software not found!'

                        mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

                if query_word[:6] == 'server':

                    key_word = query_word[:6]

                    search_server = query_word[7:]

                    if search_server != '':

                        server, software, version, users, mau, alive = db.fediquery_server_data(search_server)

                        toot_text = f'@{username}, my data for {search_server}:\n\n'

                        if server == '' or server != '' and not alive:

                            server, api = db.get_nodeinfo_endpoint(search_server)

                            if server != '' and api != '':

                                server, software, version, users, mau, posts, alive = get_server_data(server, api)

                            else:

                                api = peerapi(search_server) # get peer nodeinfo's URL

                                if api != None:

                                    server, software, version, users, mau, posts, alive = get_server_data(search_server, api)

                        if alive:

                            toot_text += f'server: {server}\nsoftware: :{software}:\nversion: {version}\nMAU: {int(mau):,}\nusers: {int(users):,}\nalive: {alive}'

                        else:

                            toot_text += 'server not found!'

                        mastodon.status_post(toot_text, in_reply_to_id=status_id,visibility=visibility)

def get_server_data(server, api):

    try:

        server, soft, version, users, mau, posts, alive = ndi.getdata(server, api)

        if soft != '':

            if soft == 'mastodon':

                try:

                    response = httpx.get(f'https://{server}/api/v1/instance', headers = setup.user_agent, timeout=3)

                    if response.status_code == 200:

                        uri = response.json()['uri']

                        if uri == server:

                            db.write_alive_server(server, soft, version, users, mau, posts, alive)

                        else:

                            alive = False

                            db.write_not_alive_server(server)

                except:

                    pass

            else:

                db.write_alive_server(server, soft, version, users, mau, posts, alive)

        else:

            db.write_not_alive_server(server)

        return server, soft, version, users, mau, posts, alive

    except:

        pass

def peerapi(peer):

    api = peers.getapi(peer) # get peer nodeinfo's URL

    if api != '':

        db.write_server(peer, api)

        print(f'server: {peer}, api: {api}')

        return api

# main

if __name__ == '__main__':

    setup = Setup()

    peers = Peers()

    mastodon = Mastodon(
            access_token = setup.mastodon_app_token,
            api_base_url= setup.mastodon_hostname
            )

    db = Database()

    ndi = Nodeinfo()

    mastodon.stream_user(Listener())
