import os
from app.libraries.setup import Setup
from app.libraries.database import Database
from app.libraries.peers import Peers
from app.libraries.nodeinfo import Nodeinfo
import time
import ray

ray.init(num_cpus = os.cpu_count()) # Specify this system CPUs.

def print_runtime(input_data, start_time):
    print(f'Runtime: {time.time() - start_time:.2f} seconds, data:')
    print(*input_data, sep="\n")

@ray.remote
def peerapi(peer):

    api = peers.getapi(peer) # get peer nodeinfo's URL

    if api != '':

        db.write_server(peer, api)

        server_peers.remote(peer)

    #else:

    #    db.write_deadserver(peer)

    return peer, api

@ray.remote
def server_peers(server):

    peers_list = peers.getpeers(server) # check and get peers if available

    if peers_list != None: # if peers list is available

        print(f'\n{server}: {len(peers_list)} peers\n')

        has_peers = True

        db.update_peer(server, has_peers, len(peers_list)) #update peer

        for peer in peers_list:

            found = db.check_peer(peer)

            if not found:

                peerapi.remote(peer)

            #return peer, api

if __name__ == '__main__':

    db = Database()

    setup = Setup()

    peers = Peers()

    peers_list = peers.getpeers(setup.mastodon_hostname) #get peers from Mastodon host

    start = time.time()
    object_references = [
        peerapi.remote(peer) for peer in peers_list
    ]
    all_data = []

    while len(object_references) > 0:
        finished, object_references = ray.wait(
            object_references, timeout=3.0
        )
        data = ray.get(finished)

        print_runtime(data, start)

        all_data.extend(data)

    print_runtime(all_data, start)
