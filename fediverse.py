import os
from app.libraries.setup import Setup
from app.libraries.database import Database
from app.libraries.peers import Peers
from app.libraries.nodeinfo import Nodeinfo
from app.libraries.graphit import Graphit
from mastodon import Mastodon
import httpx
import time
import ray
import pdb

ray.init(num_cpus = os.cpu_count()) # Specify this system CPUs.

def print_runtime(input_data, start_time):
    print(f'Runtime: {time.time() - start_time:.2f} seconds, data:')
    print(*input_data, sep="\n")

@ray.remote
class DataTracker:
    def __init__(self):
        self._counts = 0

    def increment(self):
        self._counts += 1

    def counts(self):
        return self._counts

@ray.remote
def get_nodeinfo(server, tracker):

    tracker.increment.remote()

    if server == 'pawoo.org':
        return
    server, api = db.get_nodeinfo_endpoint(server)
    
    try:

        server, soft, version, users, mau, posts, alive = ndi.getdata(server, api)

        if soft != '':

            if soft == 'mastodon':

                try:

                    with httpx.Client(headers=setup.user_agent, timeout=3) as client:

                        response = client.get(f'https://{server}/api/v1/instance')

                    try:

                        uri = response.json()['uri']

                        if isinstance(uri, list):

                            if uri == server:

                                db.write_alive_server(server, soft, version, users, mau, posts, alive)

                            else:

                                db.delete_server(server)

                    except:

                        pass

                except httpx.RequestError as exc:

                    pass

                except httpx.HTTPStatusError as exc:

                    pass

                except httpx.DecodingError as exc:

                    pass

            else:

                db.write_alive_server(server, soft, version, users, mau, posts, alive)

            return server, soft, version, users, mau, posts, alive

        else:

            return server

    except:

        pass

if __name__ == '__main__':

    db = Database()

    setup = Setup()

    peers = Peers()

    ndi = Nodeinfo()

    mastodon = Mastodon(
        access_token = setup.mastodon_app_token,
        api_base_url= setup.mastodon_hostname
        )

    saved_servers = db.get_saved_servers()

    tracker = DataTracker.remote()

    start = time.time()
    object_references = [
        get_nodeinfo.remote(server, tracker) for server in saved_servers
    ]
    all_data = []

    while len(object_references) > 0:

        finished, object_references = ray.wait(
            object_references, timeout=3.0
        )

        data = ray.get(finished)

        counter = ray.get(tracker.counts.remote())

        print(f'\n\n{counter} of {len(saved_servers)}')

        for v in data:

            if type(v) == tuple:

                fails, fails_count = db.get_server_fails(v[0])

                db.update_server_fails(v[0], 0, fails_count + 0)

                print_runtime(data, start)

            elif type(v) == str:

                fails, fails_count = db.get_server_fails(data[0])

                if fails < 7:

                    print(f"{data[0]} is down (fails: {fails + 1})")

                    db.update_server_fails(data[0], fails + 1, fails_count + 1)

                else:

                    print(f"** deleting {data[0]}\n")
                    db.delete_server(data[0])

            else:

                pass

        all_data.extend(data)

    print_runtime(all_data, start)

    # get current total servers and users, get users from every software

    total_servers = 0

    total_users = 0

    soft_total_project, soft_total_users, soft_total_mau, soft_total_servers, total_servers, total_users, total_mau = db.soft_totals()

    i = 0

    while i < len(soft_total_project):

        db.write_top_soft(soft_total_project[i], soft_total_users[i], soft_total_mau[i], soft_total_servers[i])

        i += 1

    # get last check values and write current total ones

    evo_servers, evo_users, evo_mau = db.last_values(total_servers, total_users, total_mau)

    # write evo values

    db.write_evo(evo_servers, evo_users, evo_mau)

    graph = Graphit()

    # get max servers and mau

    graph.servers_max, graph.users_max, graph.mau_max = db.max()

    # get plots

    graph.servers, graph.users, graph.mau, graph.global_day, graph.global_servers, graph.global_users, graph.global_mau, graph.global_week, graph.global_week_users, graph.global_week_mau = db.get_plots()

    graph.generate()

    graph.generate_weeks()

    ###############################################################################
    # P O S T !

    post_text = "#fediverse alive servers stats" + " \n"

    post_text += "\n"

    if evo_servers >= 0:

        post_text += f"servers: {total_servers:,} ({evo_servers:,}, max: {graph.servers_max:,})\n"

    elif evo_servers < 0:

        post_text += f"servers: {total_servers:,} ({evo_servers:,}, max: {graph.servers_max:,})\n"

    if evo_users >= 0:

        post_text += f"users: {total_users:,} ({evo_users:,}, max: {graph.users_max:,})\n"

    elif evo_users < 0:

        post_text += f"users: {total_users:,} ({evo_users:,}, max: {graph.users_max:,})\n"

    if evo_mau >= 0:

        post_text += f"MAU: {total_mau:,} ({evo_mau:,}, max: {graph.mau_max:,})\n"

    elif evo_mau < 0:

        post_text += f"MAU: {total_mau:,} ({evo_mau:,}, max: {graph.mau_max:,})\n"

    post_text += "\ntop five projects (users / MAU / servers):\n\n"

    i = 0

    while i < 5:

        project_soft = soft_total_project[i]

        project_mau = soft_total_mau[i]

        project_users = soft_total_users[i]

        project_servers = soft_total_servers[i]

        len_pr_soft = len(project_soft)

        if project_soft == 'activity-relay':

            project_soft = 'activityrelay'

        post_text += f":{project_soft}: {project_users:,} / {project_mau:,} / {project_servers:,}\n"

        i += 1

    print(f"Posting...\n{post_text}")

    servers_image_id = mastodon.media_post('app/graphs/global_servers.png', "image/png", description='servers graph').id

    users_image_id = mastodon.media_post('app/graphs/global_users.png', "image/png", description='users graph').id

    mau_image_id = mastodon.media_post('app/graphs/global_mau.png', "image/png", description='mau graph').id

    weeks_image_id = mastodon.media_post('app/graphs/global_weeks.png', "image/png", description='weeks graph').id 

    mastodon.status_post(post_text, in_reply_to_id=None, media_ids={servers_image_id, users_image_id, mau_image_id, weeks_image_id})
