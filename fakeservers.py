import os
from app.libraries.database import Database
from app.libraries.peers import Peers
import psycopg2
import time
import ray
import pdb

ray.init(num_cpus = os.cpu_count()) # Specify this system CPUs.

def get_servers(min_peers_value):

    peers_servers = []

    total_peers = 0

    try:

        conn = None

        conn = psycopg2.connect(database=db.mau_db, user=db.mau_db_user, password = db.mau_db_user_password, host="/var/run/postgresql", port="5432")

        cur = conn.cursor()

        # getservers with peers list

        cur.execute("select server, peers from servers where has_peers and peers > " + min_peers_value)

        rows = cur.fetchall()

        for row in rows:

            peers_servers.append(row[0])

            total_peers = total_peers + row[1]

        cur.close()

        print(f"servers with peers: {len(peers_servers)}\ntotal peers: {total_peers}")

    except (Exception, psycopg2.DatabaseError) as error:

        print(error)

    finally:

        if conn is not None:

            conn.close()

    return peers_servers, total_peers

def print_runtime(input_data, start_time):
    print(f'Runtime: {time.time() - start_time:.2f} seconds, data:')
    print(*input_data, sep="\n")

@ray.remote
class DataTracker:
    def __init__(self):
        self._counts = 0

    def increment(self):
        self._counts += 1

    def counts(self):
        return self._counts

@ray.remote
def get_peers(server, tracker):

    fakes = 0

    tracker.increment.remote()

    peers_list = peers.getpeers(server) # check and get peers if available

    if peers_list != None: # if peers list is available

        for peer in peers_list:

            if fake_domain in peer:

                fakes += 1

    return server, len(peers_list) if peers_list != None else None, fakes,  round((fakes*100)/len(peers_list),2) if fakes != 0 else 0

if __name__ == '__main__':

    fake_domain = input("\nsearch fake domain? (in ex. activitypub-troll.cf): ")  

    min_peers_value = input("\nminimal number of peers? (in ex. 100): ") 

    db = Database()

    peers = Peers()
 
    peers_servers, total_peers = get_servers(min_peers_value)

    tracker = DataTracker.remote()

    start = time.time()

    object_references = [
        get_peers.remote(server, tracker) for server in peers_servers
    ]
    all_data = []

    while len(object_references) > 0:

        finished, object_references = ray.wait(
            object_references, timeout=3.0
        )
        data = ray.get(finished)

        counter = ray.get(tracker.counts.remote())

        print(f'\n\n{counter} of {len(peers_servers)}')

        print_runtime(data, start)

        all_data.extend(data)

    for server in all_data:

        print(server)
        db.savefake(server[0], server[1], fake_domain, server[2], server[3])

    servers, fakes = db.get_fakes()

    print(f"\n\nservers peering with {fake_domain}: {servers}\n{fake_domain} total: {fakes} times.\n") 


