import unidecode
import re
import pdb

class Mentions:

    name = 'Mentions'

    def __init__(self, mastodon=None):

        self.mastodon = mastodon

    def get_data(self, mention):

        notification_id = mention.id

        account_id = mention.account.id

        username = mention.account.acct

        status_id = mention.status.id

        text  = mention.status.content

        visibility = mention.status.visibility

        reply, query = self.get_question(text)

        return reply, query, username, status_id, visibility

    def get_question(self, text):

        reply = False

        keyword = '' 

        content = self.cleanhtml(text)

        content = self.unescape(content)

        try:

            start = content.index("@")
            end = content.index(" ")
            if len(content) > end:

                content = content[0: start:] + content[end +1::]

            cleanit = content.count('@')

            i = 0
            while i < cleanit :

                start = content.rfind("@")
                end = len(content)
                content = content[0: start:] + content[end +1::]
                i += 1

            content = content.lower()
            question = content

            if unidecode.unidecode(question)[:4] == 'soft' or unidecode.unidecode(question)[:6] == 'server':

                reply = True

        except:

            return (reply, "")

        return (reply, question)

    @staticmethod
    def cleanhtml(raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    @staticmethod
    def unescape(s):
        s = s.replace("&apos;", "'")
        return s
