import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter, StrMethodFormatter
import numpy as np
import pandas as pd
import pdb

SMALL_SIZE = 6
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
plt.style.use('seaborn-v0_8-dark')

mdates.set_epoch('2000-01-01T00:00:00')

class Graphit():

    name = 'fediverse data graph library'

    def __init__(self,
            servers=None,
            servers_max=None,
            users=None,
            users_max=None,
            mau=None,
            mau_max=None,
            global_day=None,
            global_servers=None,
            global_users=None,
            global_mau=None,
            global_week=None,
            global_week_users=None,
            global_week_mau=None
            ):

        self.servers = servers
        self.servers_max = servers_max
        self.users = users
        self.users_max = users_max
        self.mau = mau
        self.mau_max = mau_max
        self.global_day = global_day
        self.global_servers = global_servers
        self.global_users = global_users
        self.global_mau = global_mau

        self.global_week = global_week
        self.global_week_users = global_week_users
        self.global_week_mau = global_week_mau

    def graph(self):

        # generate graphs

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.servers[6], self.servers[5], self.servers[4], self.servers[3], self.servers[2], self.servers[1], self.servers[0]], marker='o', color='mediumseagreen')

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.servers_max, self.servers_max, self.servers_max, self.servers_max, self.servers_max, self.servers_max, self.servers_max], color='red')

        plt.title('fediverse: total alive servers (max: ' + str(f"{self.servers_max:,}" + ')'), loc='right', color='blue')

        plt.xlabel('Last seven days')

        plt.ylabel('fediverse alive servers')

        plt.grid(visible=True)

        plt.legend(('servers', 'max'), shadow=True, loc=(0.01, 1.00), handlelength=1.5, fontsize=10)

        plt.savefig('servers.png')

        plt.close()

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.users[6], self.users[5], self.users[4], self.users[3], self.users[2], self.users[1], self.users[0]], marker='o', color='royalblue')

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.users_max, self.users_max, self.users_max, self.users_max, self.users_max, self.users_max, self.users_max], color='red')

        plt.title('fediverse: total users (max: ' + str(f"{self.users_max:,}" + ')'), loc='right', color='royalblue')

        plt.legend(('users', 'max'), shadow=True, loc=(0.01, 0.80), handlelength=1.5, fontsize=10)

        plt.xlabel('Last seven days')

        plt.ylabel('users')

        plt.grid(visible=True)

        plt.savefig('users.png')

        plt.close()

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.mau[6], self.mau[5], self.mau[4], self.mau[3], self.mau[2], self.mau[1], self.mau[0]], marker='o', color='royalblue')

        plt.plot([-6, -5, -4, -3, -2, -1, 0], [self.mau_max, self.mau_max, self.mau_max, self.mau_max, self.mau_max, self.mau_max, self.mau_max], color='red')

        plt.title('fediverse: total MAU (max: ' + str(f"{self.mau_max:,}" + ')'), loc='right', color='royalblue')

        plt.legend(('mau', 'max'), shadow=True, loc=(0.01, 0.80), handlelength=1.5, fontsize=10)

        plt.xlabel('Last seven days')

        plt.ylabel('MAU')

        plt.grid(visible=True)

        plt.savefig('mau.png')

        plt.close()

    def generate(self):

        df = pd.DataFrame(
                {'date': np.array(self.global_day),
                'servers': np.array(self.global_servers),
                'users': np.array(self.global_users),
                'mau': np.array(self.global_mau)})

        df['date'] = pd.to_datetime(df['date'])

        # servers

        fig, ax = plt.subplots()

        server_min = 0

        ax.fill_between(df.date, server_min, df.servers, alpha=0.7, label='Registered', color='darkgreen')

        ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))

        plt.tick_params(rotation=45)

        ax.set_title("fediverse's total servers")

        ax.set_xlabel('date')

        ax.set_ylabel('servers')

        ax.grid(visible=True)

        ax.label_outer()

        plt.savefig('app/graphs/global_servers.png')

        # users

        fig, ax = plt.subplots()

        users_min = 0

        ax.fill_between(df.date, users_min, df.users, alpha=0.7, label='Registered', color='blue')

        ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))

        plt.tick_params(rotation=45)

        ax.set_title("fediverse's registered users")

        ax.set_xlabel('date')

        ax.set_ylabel('users')

        ax.grid(visible=True)

        plt.savefig('app/graphs/global_users.png')

        # mau

        fig, ax = plt.subplots()

        ax.plot(df.date, df.mau, label='MAU', color='blue')

        ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))

        plt.tick_params(rotation=45)

        ax.set_title("fediverse's Monthly Active Users")
        
        ax.set_xlabel('date')

        ax.set_ylabel('users')

        ax.grid(visible=True)

        plt.savefig('app/graphs/global_mau.png')

        plt.close()

    def generate_weeks(self):

        df = pd.DataFrame(
                {'week': np.array(self.global_week),
                'week_users': np.array(self.global_week_users),
                'week_mau': np.array(self.global_week_mau)})

        df['week'] = pd.to_datetime(df['week'])

        # weeks

        fig, ax = plt.subplots()

        ax.plot(df.week, df.week_users, label='users', color='green')

        ax.plot(df.week, df.week_mau, label='users', color='blue')

        ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))

        plt.tick_params(rotation=45)

        ax.set_title("fediverse's weekly users & MAU")

        ax.set_xlabel('week')

        ax.set_ylabel('users')

        ax.grid(visible=True)

        plt.savefig('app/graphs/global_weeks.png')

        plt.close()



