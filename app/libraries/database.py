import os
import sys
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import uuid
from datetime import datetime
import pytz
import pdb

tz = pytz.timezone('Europe/Madrid')

class Database():

    name = 'fediverse database library'

    def __init__(self, config_file=None, mau_db=None, mau_db_user=None, mau_db_user_password=None):

        self.config_file = "config/db_config.txt"
        self.mau_db = self.__get_parameter("mau_db", self.config_file)
        self.mau_db_user = self.__get_parameter("mau_db_user", self.config_file)
        self.mau_db_user_password = self.__get_parameter("mau_db_user_password", self.config_file)

        db_setup = self.__check_dbsetup(self)

        if not db_setup:

            self.mau_db = input("\nMau database name: ")
            self.mau_db_user = input("\nMau database user: ")
            self.mau_db_user_password = input("\nMau database user password: ")

            self.__createdb(self)  
            self.__create_config(self)
            self.__write_config(self)

    def get_nodeinfo_endpoint(self, server):

        try:

            conn = None

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("select server, api from servers where server=(%s)", (server,))

            row = cur.fetchone()

            if row != None:

                server = row[0]

                api = row[1]

            else:

                server = ''

                api = ''

            cur.close()

            return (server, api)

        except (Exception, psycopg2.DatabaseError) as error:

            sys.exit(error)

        finally:

            if conn is not None:

                conn.close()

    def save_time(self, program, start, finish):

        insert_sql = "INSERT INTO execution_time(program, start, finish) VALUES(%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (program, start, finish,))

            cur.execute("UPDATE execution_time SET start=(%s), finish=(%s) where program=(%s)", (start, finish, program))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def check_peer(self, server):

        found = False

        select_server_sql = 'select server from servers where server=(%s)'

        #select_dead_server_sql = 'select server from deadservers where server=(%s)'

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            # check server

            cur.execute(select_server_sql, (server,))

            row = cur.fetchone()

            if row != None:

                found = True

            #cur.execute(select_dead_server_sql, (server,))

            #row = cur.fetchone()

            #if row != None:

            #    found = True

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return found

    def get_not_updated_servers(self):

        not_updated_servers = []

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            # get saved servers list

            cur.execute("select server from servers where updated_at is null")

            rows = cur.fetchall()

            for row in rows:

                not_updated_servers.append(row[0])

            cur.close()

            print("Not updated servers: " + str(len(not_updated_servers)))

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return not_updated_servers

    def get_saved_servers(self):

        saved_servers = []

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            # get saved servers list

            cur.execute("select server from servers where api is not null")

            rows = cur.fetchall()

            for row in rows:

                saved_servers.append(row[0])

            cur.close()

            print("Saved servers: " + str(len(saved_servers)))

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return saved_servers

    def get_servers_with_peers(self):

        peers_servers = []

        total_peers = 0

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            # getservers with peers list

            cur.execute("select server, peers from servers where has_peers")

            rows = cur.fetchall()

            for row in rows:

                peers_servers.append(row[0])

                total_peers = total_peers + row[1]

            cur.close()

            print(f"servers with peers: {len(peers_servers)}\ntotal peers: {total_peers}")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return peers_servers, total_peers

    def get_dead_servers(self):

        dead_servers = []

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            # get dead servers list

            cur.execute("select server from deadservers")

            rows = cur.fetchall()

            for row in rows:

                dead_servers.append(row[0])

            cur.close()

            print("Dead servers: " + str(len(dead_servers)))

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return dead_servers

    def soft_totals(self):

        # get current total servers and users, get users from every software

        now = datetime.now()

        gettotals_sql = "select count(server), sum(users), sum(mau) from mau where alive"
        get_soft_totals_sql = "select software, sum(users) as users, sum(mau) as mau, count(server) as servers from mau where users != 0 and mau is not null and alive group by software order by mau desc"

        soft_total_project = []
        soft_total_users = []
        soft_total_mau = []
        soft_total_servers = []

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(gettotals_sql)

            row = cur.fetchone()

            total_servers = row[0]

            total_users = row[1]

            total_mau = row[2]

            cur.execute(get_soft_totals_sql)

            rows = cur.fetchall()

            for row in rows:

                soft_total_project.append(row[0])
                soft_total_users.append(row[1])
                soft_total_mau.append(row[2])
                soft_total_servers.append(row[3])

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return (soft_total_project, soft_total_users, soft_total_mau, soft_total_servers, total_servers, total_users, total_mau)

    def write_alive_server(self, server, software, version, users, mau, posts, alive):

        now = datetime.now()

        insert_sql = "INSERT INTO mau(server, software, version, users, mau, posts, alive, updated_at) VALUES(%s, %s,%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (server, software, version, users, mau, posts, alive, now))

            cur.execute("UPDATE mau SET software=(%s), version=(%s), users=(%s), mau=(%s), posts=(%s), alive=(%s), updated_at=(%s) where server=(%s)", (software, version, users, mau, posts, alive, now, server))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def write_not_alive_server(self, server):

        now = datetime.now()

        update_sql = "UPDATE mau set alive='f', updated_at=(%s) where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(update_sql, (now, server))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def write_server(self, server, api):

        now = datetime.now()

        insert_sql = "INSERT INTO servers(server, api, created_at) VALUES(%s, %s, %s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (server, api, now))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def savefake(self, server, peers, fake_domain, fakes, percent):

        now = datetime.now()

        insert_sql = "INSERT INTO fakes(server, peers, fake_domain, fakes, percent, updated_at) VALUES(%s, %s, %s, %s, %s, %s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (server, peers, fake_domain, fakes, percent, now))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def get_fakes(self):

        servers = 0

        fakes = 0

        select_servers = "select count(*) from fakes where fakes >0;"

        select_fakes = "select sum(fakes) from fakes where fakes >0"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(select_servers)

            row = cur.fetchone()

            if row != None:

                servers = row[0]

            cur.execute(select_fakes)

            row = cur.fetchone()

            if row != None:

                fakes = row[0]

            cur.close()

            return servers, fakes

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def write_deadserver(self, server):

        now = datetime.now()

        insert_sql = "INSERT INTO deadservers(server, checked_at) VALUES(%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (server, now))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def update_peer(self, server, has_peers, peers):

        now = datetime.now()

        update_sql = "UPDATE servers set has_peers=(%s), peers=(%s), updated_at=(%s) where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(update_sql, (has_peers, peers, now, server))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def update_server(self, server, api):

        now = datetime.now()

        update_sql = "UPDATE servers set server=(%s), api=(%s), updated_at=(%s) where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(update_sql, (server, api, now, server))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def delete_server(self, server):

        now = datetime.now()

        delete_sql = "delete from servers where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(delete_sql, (server,))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def get_server_fails(self, server):

        fails = 0

        fails_count = 0

        select_sql = "select fails, fails_count from servers where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(select_sql, (server,))

            row = cur.fetchone()

            if row != None:

                fails = row[0] if row[0] != None else 0

                fails_count = row[1] if row[1] != None else 0

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return fails, fails_count

    def update_server_fails(self, server, fails, fails_count):

        now = datetime.now()

        update_sql = "UPDATE servers set failed_at=(%s), fails=(%s), fails_count=(%s) where server=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(update_sql, (now, fails, fails_count, server))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def soft_totals(self):

        # get current total servers and users, get users from every software

        now = datetime.now()

        gettotals_sql = "select count(server), sum(users), sum(mau) from mau where alive"
        get_soft_totals_sql = "select software, sum(users) as users, sum(mau) as mau, count(server) as servers from mau where users != 0 and mau is not null and alive group by software order by mau desc"

        soft_total_project = []
        soft_total_users = []
        soft_total_mau = []
        soft_total_servers = []

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(gettotals_sql)

            row = cur.fetchone()

            total_servers = row[0]

            total_users = row[1]

            total_mau = row[2]

            cur.execute(get_soft_totals_sql)

            rows = cur.fetchall()

            for row in rows:

                soft_total_project.append(row[0])
                soft_total_users.append(row[1])
                soft_total_mau.append(row[2])
                soft_total_servers.append(row[3])

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return (soft_total_project, soft_total_users, soft_total_mau, soft_total_servers, total_servers, total_users, total_mau)

    def last_values(self, total_servers, total_users, total_mau):

        #  get last check values and write current total ones

        now = datetime.now()

        evo_servers = 0

        evo_users = 0

        evo_mau = 0

        select_sql = "select total_servers, total_users, total_mau from totals order by datetime desc limit 1"

        insert_sql = "INSERT INTO totals(datetime, total_servers, total_users, total_mau) VALUES(%s,%s,%s,%s)"

        try:

            conn = None

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(select_sql)

            row = cur.fetchone()

            if row is not None:

                servers_before = row[0]

                users_before = row[1]

                mau_before = row[2]

            else:

                servers_before = 0

                users_before = 0

                mau_before = 0

            cur.execute(insert_sql, (now, total_servers, total_users, total_mau))

            conn.commit()

            cur.close()

            evo_servers = total_servers - servers_before

            evo_users = total_users - users_before

            evo_mau = total_mau - mau_before

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return (evo_servers, evo_users, evo_mau)

    def write_top_soft(self, soft_total_project, soft_total_users, soft_total_mau, soft_total_servers):

        now = datetime.now()

        insert_sql = "INSERT INTO top(datetime, software, users, mau, servers) VALUES(%s,%s,%s,%s,%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (now, soft_total_project, soft_total_users, soft_total_mau, soft_total_servers))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def write_evo(self, evo_servers, evo_users, evo_mau):

        #  write evo values

        now = datetime.now()

        insert_sql = "INSERT INTO evo(datetime, servers, users, mau) VALUES(%s,%s,%s,%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (now, evo_servers, evo_users, evo_mau))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def max(self):

        # get max servers, max users and max mau

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute("select MAX(total_servers) from totals")

            row = cur.fetchone()

            if row is not None:

                max_servers = row[0]

            else:

                max_servers = 0

            cur.execute("select MAX(total_users) from totals")

            row = cur.fetchone()

            if row is not None:

                max_users = row[0]

            else:

                max_users = 0

            cur.execute("select MAX(total_mau) from totals")

            row = cur.fetchone()

            if row is not None:

                max_mau = row[0]

            else:

                max_mau = 0

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return (max_servers, max_users, max_mau)

    def get_plots(self):

        # get plots

        servers_plots = []

        users_plots = []

        mau_plots = []

        global_day = []

        global_servers = []

        global_users = []

        global_mau = []

        global_week = []

        global_week_users = []

        global_week_mau = []

        conn = None

        try:

            conn = psycopg2.connect(database=self.mau_db, user=self.mau_db_user, password = self.mau_db_user_password, host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute("select total_servers, total_users, total_mau from totals order by datetime desc limit 14")

            rows = cur.fetchall()

            for row in rows:

                servers_plots.append(row[0])

                users_plots.append(row[1])

                mau_plots.append(row[2])

            cur.execute("select distinct on (date_trunc('day', datetime)) datetime::TIMESTAMP::DATE, total_servers, total_users, total_mau from totals group by date_trunc('day', datetime), datetime")

            rows = cur.fetchall()

            for row in rows:

                global_day.append(row[0])

                global_servers.append(row[1])

                global_users.append(row[2])

                global_mau.append(row[3])

            cur.execute("select distinct on (date_trunc('week', datetime)) datetime::TIMESTAMP::DATE, total_users, total_mau from totals group by date_trunc('week', datetime), datetime")

            rows = cur.fetchall()

            for row in rows:

                global_week.append(row[0])

                global_week_users.append(row[1])

                global_week_mau.append(row[2])

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

        return (servers_plots, users_plots, mau_plots, global_day, global_servers, global_users, global_mau, global_week, global_week_users, global_week_mau)

    def get_soft_data(self, search_soft):

        try:

            conn = None

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("select count(server), sum(users), sum(mau) from mau where software=(%s) and alive", (search_soft,))

            row = cur.fetchone()

            if row != None:

                servers = row[0]

                users = row[1]

                mau = row[2]

            else:

                servers = 0

                users = 0

                mau = 0

            cur.close()

            return (servers, users, mau)

        except (Exception, psycopg2.DatabaseError) as error:

            sys.exit(error)

        finally:

            if conn is not None:

                conn.close()

    def fediquery_server_data(self, search_server):

        try:

            conn = None

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            cur.execute("select server, software, version, users, mau, alive from mau where server=(%s)", (search_server,))

            row = cur.fetchone()

            if row != None:

                server = row[0]

                software = row[1]

                version = row[2]

                users = row[3]

                if row[4] != None:

                    mau = row[4]

                else:

                    mau = 0

                alive = row[5]

            else:

                server = ''

                software = ''

                version = ''

                users = 0

                mau = 0

                alive = False

            cur.close()

            return (server, software, version, users, mau, alive)

        except (Exception, psycopg2.DatabaseError) as error:

            sys.exit(error)

        finally:

            if conn is not None:

                conn.close()

    def csv_save(self, filename):

        is_saved = False

        url_object = URL.create(
            "postgresql+psycopg2",
            username=self.mau_db_user,
            password=self.mau_db_user_password,
            host="localhost",
            database=self.mau_db,
        )

        sql = "select * from mau where alive order by mau desc"

        conn = None

        try:

            conn = sqlalchemy.create_engine(url_object)

            with conn.connect() as connection:

                dataframe = pd.read_sql_query(sql, conn)

                dataframe.to_csv(filename, sep='|', header=True, index=False)

                is_saved = True

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        return is_saved

    @staticmethod
    def __check_dbsetup(self):

        db_setup = False

        try:

            conn = None

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            db_setup = True

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        return db_setup

    @staticmethod
    def __createdb(self):

        conn = None

        try:

            conn = psycopg2.connect(dbname='postgres',
                user=self.mau_db_user, host='',
                password=self.mau_db_user_password)

            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

            cur = conn.cursor()

            print(f"Creating database {self.mau_db}. Please wait...")

            cur.execute(sql.SQL("CREATE DATABASE {}").format(
                sql.Identifier(self.mau_db))
            )
            print(f"Database {self.mau_db} created!\n")

            self.__dbtables_schemes(self)

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __dbtables_schemes(self):

        db = self.mau_db
        table = "mau"
        sql = """create table """+table+""" (
                        server varchar(200) PRIMARY KEY,
                        software varchar(50),
                        version varchar(50),
                        users int, mau int,
                        posts int,
                        alive boolean,
                        updated_at timestamptz
                        )"""
        self.__create_table(self, table, sql)

        table = "servers"
        sql = """create table """+table+""" (
                        server varchar(200) PRIMARY KEY,
                        api varchar(50),
                        created_at timestamptz,
                        updated_at timestamptz,
                        failed_at timestamptz,
                        fails int,
                        fails_count int,
                        has_peers boolean default false,
                        peers int
                        )"""
        self.__create_table(self, table, sql)

        table = "totals"
        sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, total_servers INT, total_users INT, total_mau INT)"
        self.__create_table(self, table, sql)

        table = "evo"
        sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, servers INT, users INT, mau INT)"
        self.__create_table(self, table, sql)

        table = "top"
        sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, software varchar(50), users INT, mau INT, servers INT)"
        self.__create_table(self, table, sql)

        table = "fakes"
        sql = "create table "+table+" (server varchar PRIMARY KEY, peers int, fake_domain varchar, fakes INT, percent float, updated_at timestamptz)"
        self.__create_table(self, table, sql)

    @staticmethod
    def __create_table(self, table, sql):

        conn = None

        try:

            conn = psycopg2.connect(database = self.mau_db, user = self.mau_db_user, password = self.mau_db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            print(f"Creating table {table}")

            cur.execute(sql)

            conn.commit()

            print(f"Table {table} created!\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def __get_parameter(self, parameter, config_file):

        if not os.path.isfile(config_file):
            print(f"File {config_file} not found..")
            return

        with open( config_file ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

        print(f"{config_file} Missing parameter {parameter}")

        sys.exit(0)

    @staticmethod
    def __create_config(self):

        if not os.path.exists('config'):

            os.makedirs('config')

        if not os.path.exists(self.config_file):

            print(self.config_file + " created!")
            with open(self.config_file, 'w'): pass

    @staticmethod
    def __write_config(self):

        with open(self.config_file, 'a') as the_file:

            the_file.write(f'mau_db: {self.mau_db}\nmau_db_user: {self.mau_db_user}\nmau_db_user_password: {self.mau_db_user_password}')
            print(f"adding parameters to {self.config_file}\n")


