import time
from datetime import datetime
import os
import json
import sys
import os.path
from app.libraries.setup import Setup
from app.libraries.database import Database
from urllib.parse import urlparse, urlunparse
import httpx

def convert_idna_address(url: str) -> str:
    parsed_url = urlparse(url)
    return urlunparse(
        parsed_url._replace(netloc=parsed_url.netloc.encode("idna").decode("ascii"))
    )

class Peers():

    name = "Get peers from server"

    def __init__(self, server=None, db=None, setup=None):

        self.server = server
        self.db = Database()
        self.setup = Setup()

    def getpeers(self, server):

        try:

            with httpx.Client(headers=self.setup.user_agent, timeout=3) as client:


                res = client.get(f'https://{server}/{self.setup.peers_api}')

                try:

                    peers = res.json()

                    if isinstance(peers, list):

                        self.db.update_peer(server, True, len(peers))

                        return peers

                    else:

                        return None

                except:

                    pass

        except httpx.RequestError as exc:

            pass

        except httpx.HTTPStatusError as exc:

            pass

        except httpx.DecodingError as exc:

            pass

        print('*** Not peers api!')
        return None

    def getapi(self, server):

        if server.find(".") == -1:
            return
        if server.find("@") != -1:
            return
        if server.find("/") != -1:
            return
        if server.find(":") != -1:
            return

        is_nodeinfo = False

        nodeinfo = ''

        url = 'https://' + server

        converted_url = convert_idna_address(url)

        try:

            with httpx.Client(headers=self.setup.user_agent, timeout=3) as client:

                response = client.get(converted_url + '/.well-known/nodeinfo')

                if response.status_code == 200:

                    try:

                        response_json = response.json()

                        if len(response_json['links']) == 1:

                            start = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][1]
                            end = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][2]

                            server = response_json['links'][0]['href'][start+1:end]

                            server_idx = response_json['links'][0]['href'].index(server)

                            nodeinfo = response_json['links'][0]['href'][server_idx:].replace(server, '')


                        elif len(response_json['links']) == 2:

                            start = [pos for pos, char in enumerate(response_json['links'][1]['href']) if char == '/'][1]
                            end = [pos for pos, char in enumerate(response_json['links'][1]['href']) if char == '/'][2]

                            server = response_json['links'][1]['href'][start+1:end]

                            server_idx = response_json['links'][1]['href'].index(server)

                            nodeinfo = response_json['links'][1]['href'][server_idx:].replace(server, '')

                        elif len(response_json['links']) > 2: #because Worpress's ActivityPub plugin by Matthias Pfefferle & Automattic returns four links

                            start = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][1]
                            end = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][2]

                            server = response_json['links'][0]['href'][start+1:end]

                            server_idx = response_json['links'][0]['href'].index(server)

                            nodeinfo = response_json['links'][0]['href'][server_idx:].replace(server, '')

                    except:

                        pass

        except httpx.RequestError as exc:

            pass

        except httpx.HTTPStatusError as exc:

            pass

        except httpx.DecodingError as exc:

            pass

        return nodeinfo

    def updateapi(self, server):

        if server.find(".") == -1:
            return
        if server.find("@") != -1:
            return
        if server.find("/") != -1:
            return
        if server.find(":") != -1:
            return

        nodeinfo = ''

        url = 'https://' + server

        try:

            with httpx.Client(headers=self.setup.user_agent, timeout=3) as client:

                response = client.get(url + '/.well-known/nodeinfo')

                if response.status_code == 200:

                    try:

                        response_json = response.json()

                        if len(response_json['links']) == 1:

                            start = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][1]
                            end = [pos for pos, char in enumerate(response_json['links'][0]['href']) if char == '/'][2]

                            server = response_json['links'][0]['href'][start+1:end]

                            server_idx = response_json['links'][0]['href'].index(server)

                            nodeinfo = response_json['links'][0]['href'][server_idx:].replace(server, '')

                        elif len(response_json['links']) == 2:

                            start = [pos for pos, char in enumerate(response_json['links'][1]['href']) if char == '/'][1]
                            end = [pos for pos, char in enumerate(response_json['links'][1]['href']) if char == '/'][2]

                            server = response_json['links'][1]['href'][start+1:end]

                            server_idx = response_json['links'][1]['href'].index(server)

                            nodeinfo = response_json['links'][1]['href'][server_idx:].replace(server, '')

                    except:

                        pass

        except httpx.RequestError as exc:

            pass

        except httpx.HTTPStatusError as exc:

            pass

        except httpx.DecodingError as exc:

            pass

        return server, nodeinfo
