# Fediverse Stats
This code gets all peers from Mastodon server host and then all peers from host server's peers. Goal is to collect maximum number of alive fediverse's servers and then query the nodeinfo endpoint of all of them to obtain their registered users and MAU (if their nodeinfo is providing such information).
At the end it post the results to host server bot account.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon running server.

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python fetchpeers.py`. First run will setup everything up and after that it will get maximum peers possible from Mastodon host peers list and beyond.

3. Run `python fediverse.py` to check an get all information from all peers nodeinfo endpoints. At the end it will publish the results to the configured Mastodon account.  

4. Run `python fakeservers.py` to check and get fake domains from all servers with public peers list available.

5. Use your favourite scheduling method to set `python fetchpeers.py` to run at least once a day and `python fediverse.py` to run at desired pace to publish the results. Also set `python fediquery.py` to run every minute to accept queries from any fediverse' users about any `server` or `soft`.  

6. Use your favourite scheduling method to run `python upload.py` if you want to upload alive fediverse's datasets to your configured Forgejo repository.

15.4.2023 - Added fediquery.py. It allows queries from any fediverse' user about soft and server (keywords). It replies to the asking user with its data, if any.  
28.5.2023 - Added top table to save top five softwares data.  
28.5.2023 - Added upload.py to upload alive fediverse's dataset to configured Forgejo repository.  
20.10.2023 - Added support of Wordpress's ActivityPub plugin written by Matthias Pfefferle & Automattic.  
14.5.2024 - Do not save not alive server. Save no response servers (fails) and delete them after 7 fails.  
15.5.2024 - Delete domain aliases.  
22.5.2024 - Added fakes table and fakeservers.py auxiliar tool to find fake servers.


