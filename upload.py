import os
import base64
from datetime import datetime
from app.libraries.database import Database
from app.libraries.forgejo import Forgejo, ForgejoNotFoundError
import pdb

def date_string():

    now = datetime.now()

    day = '{:02d}'.format(now.day)

    month = '{:02d}'.format(now.month)

    year = str(now.year)

    return day, month, year

if __name__ == '__main__':

    db = Database()

    if not os.path.exists('dataset'):

        os.makedirs('dataset')

    day, month, year = date_string()

    if not os.path.exists(f'dataset/{year}'):

        os.makedirs(f'dataset/{year}')

    if not os.path.exists(f'dataset/{year}/{month}'):

        os.makedirs(f'dataset/{year}/{month}')

    filename = f'dataset_{year}{month}{day}.csv'

    db.csv_save(f'dataset/{year}/{month}/{filename}')

    fgj = Forgejo()

    gituser = fgj.user()

    try:

        with open(f'dataset/{year}/{month}/{filename}', 'rb') as input_file:
            data = input_file.read()
            file = base64.b64encode(data)

        response = fgj.repo_owner_create_file(gituser.login, fgj.stats_repo, f'dataset/{year}/{month}/{filename}', gituser.email, gituser.login, "main", file, f"{year}{month}{day} fediverse's dataset")

        if 'content' in response:

            print(f'{filename} uploaded to {fgj.api_base_url}/{gituser.login}/{fgj.stats_repo}')

        else:

            print(response)

    except:

        print(response)

        pass
